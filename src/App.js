import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import Header from './containers/Header/Header';
import HomePage from './containers/HomePage/HomePage';
import About from "./containers/About/About";
import Contacts from "./containers/Contacts/Contacts";
import Services from "./containers/Services/Services";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Route path="/" component={Header}  />
                <Switch>
                    <Route path="/" exact component={HomePage}/>
                    <Route path="/about" component={About}/>
                    <Route path="/contacts" component={Contacts}/>
                    <Route path="/services" component={Services}/>
                    <Route render={() => <h1>404 Not Found</h1>}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;
