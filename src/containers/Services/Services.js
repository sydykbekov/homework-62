import React, {Component, Fragment} from 'react';
import {NavLink} from "react-router-dom";

class Services extends Component {
    render() {
        return (
            <Fragment>
                <div className="jumbotron">
                    <h1>Services</h1>
                    <h4>Total Solutions from the Ground Up</h4>
                    <p>At Supreme, we are able to transform a barren stretch of land into a fully operational,
                        completely secure, self-sustaining outpost in a matter of weeks. Our comprehensive abilities
                        enable us to clear, build, equip and maintain facilities in the most austere and challenging
                        environments quickly and efficiently. We provide the shelter, infrastructure and life-sustaining
                        support services that allow defense forces, government agencies and commercial sector clients to
                        focus on accomplishing their missions.</p>
                    <h4>Our Site Service Solutions include:</h4>
                    <ul>
                        <li>Catering</li>
                        <li>Facilities Management</li>
                        <li>Retail Services</li>
                        <li>Hotels</li>
                        <li>Construction</li>
                    </ul>
                    <p>Our Site Services capabilities are augmented by our global supply chain for food and fuels, which
                        allows us to deliver total solutions that are unmatched in scale and scope. We are a complete
                        one-stop-shop for organizations that operate in demanding regions. </p>
                    <p><NavLink to="/" className="btn btn-primary btn-lg" role="button">Back</NavLink></p>
                </div>
            </Fragment>
        )
    }
}

export default Services;
