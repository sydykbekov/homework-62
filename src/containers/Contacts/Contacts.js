import React, {Component, Fragment} from 'react';
import {NavLink} from "react-router-dom";

class Contacts extends Component {
    render() {
        return (
            <Fragment>
                <div className="jumbotron">
                    <h1>Contacts</h1>
                    <p>General: info@aace.org <br/>
                        Conferences: conf@aace.org<br/>
                        Conference Registrations, Journal Subscriptions, or Payments: business@aace.org<br/>
                        Journal Authors: pubs@aace.org<br/>
                        LearnTechLib Digital Library: info@LearnTechLib.org<br/>
                        Technical Support: tech@aace.org</p>
                    <p><NavLink to="/" className="btn btn-primary btn-lg" role="button">Back</NavLink></p>
                </div>
            </Fragment>
        )
    }
}

export default Contacts;