import React, {Component, Fragment} from 'react';
import './About.css';
import {NavLink} from "react-router-dom";

class About extends Component {
    render() {
        return (
            <Fragment>
                <div className="jumbotron">
                    <h1>About us</h1>
                    <p>Mission Statement: The Society for Information Technology and Teacher Education (SITE) is an
                        international, academic association of teacher educators, researchers, practitioners and
                        collaborating organizations across multiple disciplines. SITE creates and disseminates knowledge
                        enhancing teacher education through the use of technology across a global context. SITE promotes
                        research, scholarship, and innovation across its membership. It is the only organization solely
                        focused on integrating technology into teacher education.</p>
                    <p><NavLink to="/" className="btn btn-primary btn-lg" role="button">Back</NavLink></p>
                </div>
            </Fragment>
        )
    }
}

export default About;
