import React, {Component, Fragment} from 'react';
import './HomePage.css';
import pic1 from './img/img-1.jpeg';
import pic2 from './img/img-2.jpg';
import pic3 from './img/img-3.jpg';
import pic4 from './img/img-4.jpg';
import pic5 from './img/img-5.jpg';
import pic6 from './img/img-6.jpg';
import cat from './img/cat.jpg';
import {Link} from "react-router-dom";


class HomePage extends Component {

    render() {
        return (
            <Fragment>
                <div className="container">
                    <div className="row">
                        <div className="page-header">
                            <h1>Welcome to Modern Business</h1>
                        </div>
                        <div className="col-sm-6 col-md-4">
                            <ol className="breadcrumb">
                                <li><span className="glyphicon glyphicon-ok"/>Bootstrap v3.2.0</li>
                            </ol>
                            <div className="thumbnail">
                                <div className="caption">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni eos pariatur
                                        numquam quidem fugit dolorum officia vitae ratione optio voluptatibus fugiat
                                        aliquam neque accusamus, quasi, recusandae fuga nam, quia autem!</p>
                                    <p><Link to="/" className="btn btn-default" role="button">Learn More</Link></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-4">
                            <ol className="breadcrumb">
                                <li><span className="glyphicon glyphicon-gift"/>Free & Open Source</li>
                            </ol>
                            <div className="thumbnail">
                                <div className="caption">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni eos pariatur
                                        numquam quidem fugit dolorum officia vitae ratione optio voluptatibus fugiat
                                        aliquam neque accusamus, quasi, recusandae fuga nam, quia autem!</p>
                                    <p><Link to="/" className="btn btn-default" role="button">Learn More</Link></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-4">
                            <ol className="breadcrumb">
                                <li><span className="glyphicon glyphicon-dashboard"/>Easy to Use</li>
                            </ol>
                            <div className="thumbnail">
                                <div className="caption">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni eos pariatur
                                        numquam quidem fugit dolorum officia vitae ratione optio voluptatibus fugiat
                                        aliquam neque accusamus, quasi, recusandae fuga nam, quia autem!</p>
                                    <p><Link to="/" className="btn btn-default" role="button">Learn More</Link></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="page-header">
                            <h2>Portfolio Heading</h2>
                        </div>
                        <div className="col-xs-6 col-md-4">
                            <Link to="/" className="thumbnail portfolio">
                                <img src={pic1} alt="pic"/>
                            </Link>
                        </div>
                        <div className="col-xs-6 col-md-4">
                            <Link to="/" className="thumbnail portfolio">
                                <img src={pic2} alt="pic"/>
                            </Link>
                        </div>
                        <div className="col-xs-6 col-md-4">
                            <Link to="/" className="thumbnail portfolio">
                                <img src={pic3} alt="pic"/>
                            </Link>
                        </div>
                        <div className="col-xs-6 col-md-4">
                            <Link to="/" className="thumbnail portfolio">
                                <img src={pic4} alt="pic"/>
                            </Link>
                        </div>
                        <div className="col-xs-6 col-md-4">
                            <Link to="/" className="thumbnail portfolio">
                                <img src={pic5} alt="pic"/>
                            </Link>
                        </div>
                        <div className="col-xs-6 col-md-4">
                            <Link to="/" className="thumbnail portfolio">
                                <img src={pic6} alt="pic"/>
                            </Link>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="page-header">
                            <h2>Modern Business Features</h2>
                        </div>
                        <div className="col-xs-12 col-md-6 text">
                            <p>The Modern Business template by Start Bootstrap includes:</p>
                            <ul>
                                <li>Bootstrap v3.2.0</li>
                                <li>jQuery v1.11.0</li>
                                <li>Font Awesome v4.1.0</li>
                                <li>Working PHP contact form with validation</li>
                                <li>Unstyled page elements for easy customization</li>
                                <li>17 HTML pages</li>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae officiis, numquam
                                sit harum aperiam? Maxime quasi ab assumenda quibusdam quod dolores molestias expedita
                                quae quis, facilis perspiciatis nostrum omnis cumque.</p>
                        </div>
                        <div className="col-xs-12 col-md-6">
                            <Link to="/" className="thumbnail cat">
                                <img src={cat} alt="pic"/>
                            </Link>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="page-header" />
                    <div className="jumbotron">
                        <div className="row">
                            <div className="col-xs-12 col-md-8">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi a corporis
                                    voluptate odit, error suscipit doloremque totam quis qui necessitatibus eum dolor
                                    blanditiis veniam saepe ipsum obcaecati esse nulla ea.</p>
                            </div>
                            <div className="col-xs-12 col-md-4">
                                <button type="submit" className="btn btn-default call-action">Call to Action</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container footer">
                    <div className="page-header"/>
                    <p>Copyright &#169; Your Website 2014</p>
                </div>
            </Fragment>
        )
    }
}

export default HomePage;