import React, {Component, Fragment} from 'react';
import './Header.css';
import promo_1 from '../Header/img/nature.gif';
import promo_2 from '../Header/img/mountain-and-trees.jpeg';
import promo_3 from '../Header/img/mountains.jpeg';
import {NavLink} from "react-router-dom";

class Header extends Component {
    render() {
        return (
            <Fragment>
                <nav className="navbar navbar-inverse">
                    <div className="container">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"/>
                                <span className="icon-bar"/>
                                <span className="icon-bar"/>
                            </button>
                            <NavLink className="navbar-brand" to="/" >Nature</NavLink>
                        </div>
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul className="nav navbar-nav navbar-right">
                                <li><NavLink to="/about">About us</NavLink></li>
                                <li><NavLink to="/services" >Services</NavLink></li>
                                <li><NavLink to="/contacts">Contacts</NavLink></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div className="container-fluid">
                    <div id="carousel-example-generic" className="carousel slide" data-ride="carousel">
                        <ol className="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" className="active"/>
                            <li data-target="#carousel-example-generic" data-slide-to="1"/>
                            <li data-target="#carousel-example-generic" data-slide-to="2"/>
                        </ol>
                        <div className="carousel-inner" role="listbox">
                            <div className="item active">
                                <img src={promo_1} alt="pic"/>
                                <div className="carousel-caption">
                                    <h3>RELAX</h3>
                                </div>
                            </div>
                            <div className="item">
                                <img src={promo_2} alt="pic"/>
                                <div className="carousel-caption">
                                    <h3>RELAX</h3>
                                </div>
                            </div>
                            <div className="item">
                                <img src={promo_3} alt="pic"/>
                                <div className="carousel-caption">
                                    <h3>RELAX</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default Header;